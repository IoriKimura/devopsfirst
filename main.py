import os


def main():
    num1 = os.environ['FIRST']
    num2 = os.environ['SECOND']
    if len(str(num1)) > len(str(num2)):
        print("num1 digits more than in num2")
        with open('query_result.txt', 'w') as f:
            f.write("num1 digits more than in num2")
            f.close()

    elif len(str(num1)) == len(str(num2)):
        print("num2 digits equal num1 digits")
        with open('query_result.txt', 'w') as f:
            f.write("num2 digits equal num1 digits")
            f.close()
    else:
        print("num2 digits more than num1 digits")
        with open('query_result.txt', 'w') as f:
            f.write("num2 digits more than num1 digits")
            f.close()


if __name__ == '__main__':
    main()
